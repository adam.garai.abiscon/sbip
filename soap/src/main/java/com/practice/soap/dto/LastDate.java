package com.practice.soap.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LastDate {

    String lastDate;
}
