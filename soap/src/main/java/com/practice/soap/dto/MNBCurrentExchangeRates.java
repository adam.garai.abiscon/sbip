package com.practice.soap.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MNBCurrentExchangeRates {

    @JsonProperty("Day")
    Day day;
}
