package com.practice.soap;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.xml.bind.JAXBElement;
import java.util.List;

public class SoapClient extends WebServiceGatewaySupport {

    public GetInfoResponseBody getInfo() {
        ObjectFactory objectFactory = new ObjectFactory();
        GetInfoRequestBody getInfoRequestBody = new GetInfoRequestBody();
        JAXBElement<GetInfoRequestBody> jaxbElement = objectFactory.createGetInfoRequestBody(getInfoRequestBody);
        JAXBElement res = (JAXBElement) getWebServiceTemplate().marshalSendAndReceive(jaxbElement,
                new SoapActionCallback("http://www.mnb.hu/webservices/MNBArfolyamServiceSoap/GetInfo"));

        return (GetInfoResponseBody) res.getValue();
    }

    public GetCurrentExchangeRatesResponseBody getCurrentExchangeRates() {
        ObjectFactory objectFactory = new ObjectFactory();
        GetCurrentExchangeRatesRequestBody getCurrentExchangeRatesRequestBody = new GetCurrentExchangeRatesRequestBody();
        JAXBElement<GetCurrentExchangeRatesRequestBody> jaxbElement = objectFactory.createGetCurrentExchangeRatesRequestBody(getCurrentExchangeRatesRequestBody);
        JAXBElement res = (JAXBElement) getWebServiceTemplate().marshalSendAndReceive(jaxbElement,
                new SoapActionCallback("http://www.mnb.hu/webservices/MNBArfolyamServiceSoap/GetCurrentExchangeRates"));

        return (GetCurrentExchangeRatesResponseBody) res.getValue();
    }

    public GetExchangeRatesResponseBody getExchangeRates(String startDateInput, String endDateInput, List<String> currencyList) {
        ObjectFactory objectFactory = new ObjectFactory();
        GetExchangeRatesRequestBody getExchangeRatesRequestBody = new GetExchangeRatesRequestBody();

        if(startDateInput != null){
            JAXBElement<String> startDate = objectFactory.createGetExchangeRatesRequestBodyStartDate(startDateInput);
            getExchangeRatesRequestBody.setStartDate(startDate);
        }

        if(endDateInput != null){
            JAXBElement<String> endDate = objectFactory.createGetExchangeRatesRequestBodyEndDate(endDateInput);
            getExchangeRatesRequestBody.setEndDate(endDate);
        }

        String currencies = String.join(",", currencyList);
        JAXBElement<String> usd = objectFactory.createGetExchangeRatesRequestBodyCurrencyNames(currencies);
        getExchangeRatesRequestBody.setCurrencyNames(usd);

        JAXBElement<GetExchangeRatesRequestBody> jaxbElement = objectFactory.createGetExchangeRates(getExchangeRatesRequestBody);

        JAXBElement res = (JAXBElement) getWebServiceTemplate().marshalSendAndReceive(jaxbElement,
                new SoapActionCallback("http://www.mnb.hu/webservices/MNBArfolyamServiceSoap/GetExchangeRates"));

        return (GetExchangeRatesResponseBody) res.getValue();
    }
}

