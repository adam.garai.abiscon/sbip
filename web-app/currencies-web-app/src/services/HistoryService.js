import axios from 'axios'
import authHeader from "./auth-header";

const HISTORY_API_BASE_URL = "http://localhost:8080/history";

class HistoryService {

    getHistory() {
        return axios.get(HISTORY_API_BASE_URL,{headers: authHeader()});
    }

}

export default new HistoryService();