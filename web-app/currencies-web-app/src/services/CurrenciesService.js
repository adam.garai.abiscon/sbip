import axios from 'axios'
import authHeader from "./auth-header";

const CURRENCIES_API_BASE_URL = "http://localhost:8080/currencies";

class CurrenciesService {

    getInfo() {
        return axios.get(CURRENCIES_API_BASE_URL + '/info');
    }

    getCurrentExchangeRates() {
        return axios.get(CURRENCIES_API_BASE_URL + '/currentExchangeRates', {headers: authHeader()});
    }

    getExchangeRates(currencyList, startDateInput, endDateInput) {
        let init = [];
        if (startDateInput) init.push(['startDateInput', startDateInput]);
        if (endDateInput) init.push(['endDateInput', endDateInput]);
        init.push(['currencyList', currencyList]);
        const params = new URLSearchParams(init);

        return axios.get(CURRENCIES_API_BASE_URL + '/exchangeRates', {params, headers: authHeader()});
    }
}

export default new CurrenciesService();