import CurrenciesService from "../../services/CurrenciesService";

const CurrentExchangeRatesActionType = {
    SUCCESS: "CURRENT_EXCHANGE_RATES_SUCCESS",
    FAIL: "CURRENT_EXCHANGE_RATES_FAIL"
};

const CurrentExchangeRatesAction = () => {
    return async (dispatch) => {
        CurrenciesService.getCurrentExchangeRates()
            .then(value => {
                dispatch({type: CurrentExchangeRatesActionType.SUCCESS, payload: value.data});
            })
            .catch((error) =>
                dispatch({
                    type: CurrentExchangeRatesActionType.FAIL, payload: error.response.data.message,
                })
            )
    };
};

export {
    CurrentExchangeRatesActionType,
    CurrentExchangeRatesAction
};
