import {AuthActionType} from "../actions/AuthAction";

const authState = {
    isLoggedIn: false,
    user: '',
};

const getAuthState = () => {
    const user = localStorage.getItem("user");
    if (user) {

        return {
            isLoggedIn: true,
            user: JSON.parse(user)
        };
    }

    return authState;
};

const newAuth = getAuthState();
const authReducer = (state = newAuth, action) => {
    switch (action.type) {

        case AuthActionType.LOGOUT_SUCCESS:
            localStorage.removeItem("user");
            return authState;

        case AuthActionType.LOGIN_SUCCESS:
            const loginAuthState = {
                isLoggedIn: true,
                user: action.payload,
            };
            localStorage.setItem("user", JSON.stringify(loginAuthState.user));
            return loginAuthState;

        default:
            return state;
    }
};

export default authReducer;
