import "./App.css";
import {Route, Switch} from "react-router-dom";
import "primereact/resources/themes/luna-green/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";
import Info from "./components/Info";
import CurrentExchangeRates from "./components/CurrentExchangeRates";
import {Card} from 'primereact/card';
import SignIn from "./components/auth/SignIn";
import Register from "./components/auth/Register";
import Header from "./components/Header";
import ExchangeRates from "./components/ExchangeRates";
import {connect} from "react-redux";
import {InfoAction} from "./redux/actions/InfoAction";
import {useEffect} from "react";
import {CurrentExchangeRatesAction} from "./redux/actions/CurrentExchangeRatesAction";
import History from "./components/History";


function App(props) {
    const {getInfo, getCurrentExchangeRates} = props;

    useEffect(() => {
        getInfo();
        getCurrentExchangeRates();
    }, [getInfo, getCurrentExchangeRates]);

    return (
        <div id="container">
            <Header/>
            <Card>
                <Switch>
                    <Route exact path="/info" component={Info}/>
                    <Route exact path="/currentExchangeRates" component={CurrentExchangeRates}/>
                    <Route exact path="/exchangeRates" component={ExchangeRates}/>
                    <Route exact path="/register" component={Register}/>
                    <Route exact path="/signIn" component={SignIn}/>
                    <Route exact path="/history" component={History}/>

                </Switch>
            </Card>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {state};
};

const mapDispatchToProps = (dispatch) => {
    return {
        getInfo: () => {
            dispatch(InfoAction());
        },
        getCurrentExchangeRates: () => {
            dispatch(CurrentExchangeRatesAction())
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
