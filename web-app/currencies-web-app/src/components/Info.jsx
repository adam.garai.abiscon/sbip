import React, {useEffect, useState} from "react";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import CurrencyFlag from "react-currency-flags";
import {getAllCountriesByCurrencyOrSymbol} from "../util/countryCurrency";
import {useSelector} from "react-redux";

const Info = () => {
    console.log("INFO COMPONENT CALLED");

    const infoState = useSelector(state => state.infoState);

    const [isLoading, setLoading] = useState(true);
    const [currentData, setCurrentData] = useState([]);
    const [historicalData, setHistoricalData] = useState([]);

    useEffect(() => {
        let curr = [];
        let his = [];

        if (infoState) {
            infoState.currencies.forEach(c => {
                try {
                    getAllCountriesByCurrencyOrSymbol('currency', c)
                    curr.push({name: c})
                } catch (error) {
                    his.push({name: c})
                }
            });
            setCurrentData(curr);
            setHistoricalData(his);
            setLoading(false);
        }

    }, [infoState]);

    if (isLoading) {
        return <div>Loading...</div>
    }

    const currentHeader = (
        <div>
            <h1 style={{margin: 0, textAlign: 'center'}}>Current currencies</h1>
            <h4 style={{margin: 5, textAlign: 'center'}}>{infoState.firstDate} : {infoState.lastDate}</h4>
        </div>
    );
    const historicalHeader = (
        <div>
            <h1 style={{margin: 0, textAlign: 'center'}}>Historical currencies</h1>
            <h4 style={{margin: 5, textAlign: 'center'}}>{infoState.firstDate} : {infoState.lastDate}</h4>
        </div>
    );

    const currentFooter = `In total there are ${currentData ? currentData.length : 0} currencies.`;
    const historicalFooter = `In total there are ${historicalData ? historicalData.length : 0} currencies.`;

    const imageBodyTemplate = (rowData) => {
        return <CurrencyFlag currency={rowData.name} size="xl"/>;
    }

    const countriesUsingCurrency = (rowData) => {
        try {
            return getAllCountriesByCurrencyOrSymbol('currency', rowData.name).join(', ');
        } catch (error) {
        }
    }

    return (
        <div>
            <DataTable value={currentData} header={currentHeader} footer={currentFooter}>
                <Column style={{width: 80, textAlign: 'center'}} header="Flag" body={imageBodyTemplate}/>
                <Column header="Currency used by countries" body={countriesUsingCurrency}/>
                <Column field="name" header="Name" sortable={true}/>
            </DataTable>
            <DataTable value={historicalData} header={historicalHeader} footer={historicalFooter}>
                <Column style={{width: 80, textAlign: 'center'}} header="Flag" body={imageBodyTemplate}/>
                <Column field="name" header="Name" sortable={true}/>
            </DataTable>
        </div>
    );
};

export default Info;