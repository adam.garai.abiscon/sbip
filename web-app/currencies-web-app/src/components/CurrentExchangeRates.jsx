import React, {useEffect, useState} from "react";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import CurrencyFlag from "react-currency-flags";
import {useSelector} from "react-redux";

const CurrentExchangeRates = () => {
    console.log("HISTORY COMPONENT CALLED");

    const currentExchangeRatesState = useSelector(state => state.currentExchangeRatesState);
    const [isLoading, setLoading] = useState(true);

    useEffect(() => {
        if (currentExchangeRatesState) {
            setLoading(false);
        }
    }, [currentExchangeRatesState]);

    if (isLoading) {
        return <div>Loading...</div>
    }
    const header = (
        <div className="table-header">
            <h1 style={{margin: 0, textAlign: 'center'}}>Current exchange rates</h1>
            <h4 style={{margin: 5, textAlign: 'center'}}>{currentExchangeRatesState.date}</h4>
        </div>
    );

    const footer = `In total there are ${currentExchangeRatesState ? currentExchangeRatesState.rates.length : 0} currencies.`;
    const imageBodyTemplate = (rowData) => {
        return <CurrencyFlag currency={rowData.currency} size="xl"/>;

    }

    const valueFix = (rowData) => {
        return parseFloat(rowData.value.replace(',', '.')).toFixed(2);
    }

    return (

        <div>
            <DataTable value={currentExchangeRatesState.rates} header={header} footer={footer}>
                <Column style={{width: 80, textAlign: 'center'}} header="Flag" body={imageBodyTemplate}/>
                <Column field="currency" header="Name" sortable={true}/>
                <Column field="value" header="Value (HUF)" body={valueFix} sortable={true}/>
                <Column field="unit" header="Unit" sortable={true}/>
            </DataTable>
        </div>
    );
};

export default CurrentExchangeRates;