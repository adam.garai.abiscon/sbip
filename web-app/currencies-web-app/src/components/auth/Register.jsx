import React, {useEffect, useState} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {classNames} from 'primereact/utils';
import {connect} from "react-redux";
import {useHistory} from "react-router-dom";
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';
import {Password} from 'primereact/password';
import {Checkbox} from 'primereact/checkbox';
import {Dialog} from 'primereact/dialog';
import {Divider} from 'primereact/divider';
import {RegisterAuthAction} from "../../redux/actions/AuthAction";


const Register = (props) => {
    const {user, register} = props;

    const defaultValues = {
        username: '',
        email: '',
        password: '',
        accept: false
    }

    const defaultErrorHandler = {
        hasError: false,
        message: ''
    }

    const [userState, setUserState] = useState(defaultValues);
    const [errorHandler, setErrorHandler] = useState(defaultErrorHandler);
    const history = useHistory();
    const {control, formState: {errors}, handleSubmit, reset} = useForm({defaultValues});

    const onSubmit = (data) => {
        setUserState(data);

        register(
            data,
            history,
            setErrorHandler
        );

        reset();
    };

    const getFormErrorMessage = (name) => {
        return errors[name] && <small className="p-error">{errors[name].message}</small>
    };

    const dialogFooter =
        <div className="p-d-flex p-jc-center">
            <Button label="OK"
                    className="p-button-text" autoFocus
                    onClick={() => setErrorHandler(defaultErrorHandler)}/>
        </div>;

    const passwordHeader = <h6>Pick a password</h6>;
    const passwordFooter = (
        <React.Fragment>
            <Divider/>
            <p className="p-mt-2">Suggestions</p>
            <ul className="p-pl-2 p-ml-2 p-mt-0" style={{lineHeight: '1.5'}}>
                <li>At least one lowercase</li>
                <li>At least one uppercase</li>
                <li>At least one numeric</li>
                <li>Minimum 8 characters</li>
            </ul>
        </React.Fragment>
    );

    return (
        <div className="form-demo">
            <Dialog visible={!errorHandler.hasError && errorHandler.message === 'OK'}
                    onHide={() => setErrorHandler(defaultErrorHandler)}
                    position="top"
                    footer={dialogFooter}
                    showHeader={false} breakpoints={{'960px': '80vw'}}
                    style={{width: '30vw'}}>
                <div className="p-d-flex p-ai-center p-dir-col p-pt-6 p-px-3">
                    <i className="pi pi-check-circle" style={{fontSize: '5rem', color: 'green'}}></i>
                    <h2>Registration Successful!</h2>
                    <p style={{lineHeight: 1.5, textIndent: '1rem'}}>
                        Your account is registered!
                    </p>
                </div>
            </Dialog>

            <Dialog visible={errorHandler.hasError} onHide={() => setErrorHandler(defaultErrorHandler)}
                    position="top"
                    footer={dialogFooter}
                    showHeader={false} breakpoints={{'960px': '80vw'}} style={{width: '30vw'}}>
                <div className="p-d-flex p-ai-center p-dir-col p-pt-6 p-px-3">
                    <i className="pi pi-times-circle" style={{fontSize: '5rem', color: 'red'}}></i>
                    <h2>Error!</h2>
                    <p style={{lineHeight: 1.5, textIndent: '1rem'}}>
                        {errorHandler.message}
                    </p>
                </div>
            </Dialog>

            <div className="p-d-flex p-jc-center">
                <div className="card">
                    <h1 className="p-text-center">Register</h1>

                    <form onSubmit={handleSubmit(onSubmit)} className="p-fluid">

                        <div className="p-field">
                            <span className="p-float-label">
                                <Controller name="username"
                                            control={control}
                                            rules={{required: 'Name is required.'}}
                                            render={({field, fieldState}) => (
                                                <InputText id={field.name} {...field} autoFocus
                                                           className={classNames({'p-invalid': fieldState.invalid})}/>
                                            )}/>
                                <label htmlFor="name"
                                       className={classNames({'p-error': errors.username})}>
                                    Name*
                                </label>
                            </span>
                            {getFormErrorMessage('name')}
                        </div>

                        <div className="p-field">
                            <span className="p-float-label p-input-icon-right">
                                <i className="pi pi-envelope"/>
                                <Controller name="email"
                                            control={control}
                                            rules={{
                                                required: 'Email is required.',
                                                pattern: {
                                                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                                    message: 'Invalid email address. E.g. example@email.com'
                                                }
                                            }}
                                            render={({field, fieldState}) => (
                                                <InputText id={field.name} {...field}
                                                           className={classNames({'p-invalid': fieldState.invalid})}/>
                                            )}/>
                                <label htmlFor="email"
                                       className={classNames({'p-error': !!errors.email})}>
                                    Email*
                                </label>
                            </span>
                            {getFormErrorMessage('email')}
                        </div>

                        <div className="p-field">
                            <span className="p-float-label">
                                <Controller name="password"
                                            control={control}
                                            rules={{required: 'Password is required.'}}
                                            render={({field, fieldState}) => (
                                                <Password id={field.name} {...field} toggleMask
                                                          className={classNames({'p-invalid': fieldState.invalid})}
                                                          header={passwordHeader} footer={passwordFooter}/>
                                            )}/>
                                <label htmlFor="password"
                                       className={classNames({'p-error': errors.password})}>
                                    Password*
                                </label>
                            </span>
                            {getFormErrorMessage('password')}
                        </div>

                        <div className="p-field-checkbox">
                            <Controller name="accept"
                                        control={control}
                                        rules={{required: true}}
                                        render={({field, fieldState}) => (
                                            <Checkbox inputId={field.name}
                                                      onChange={(e) => field.onChange(e.checked)}
                                                      checked={field.value}
                                                      className={classNames({'p-invalid': fieldState.invalid})}/>
                                        )}/>
                            <label htmlFor="accept"
                                   className={classNames({'p-error': errors.accept})}>
                                I agree to the terms and conditions*
                            </label>
                        </div>

                        <Button type="submit" label="Submit" className="p-mt-2"/>
                    </form>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        register: (userState, history, setErrorHandler) => {
            dispatch(RegisterAuthAction(userState, history, setErrorHandler));
        },
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Register);