import {Button} from "primereact/button";
import {Column} from "primereact/column";
import {DataTable} from "primereact/datatable";
import React, {useEffect, useState} from "react";
import XLSX from 'xlsx'
import HistoryService from "../services/HistoryService";

const History = () => {
    //console.log("HISTORY COMPONENT CALLED");

    const [isLoading, setLoading] = useState(true);
    const [history, setHistory] = useState([]);

    useEffect(() => {
        async function fetchHistory() {
            let resp = await HistoryService.getHistory();
            setHistory(resp.data);
            setLoading(false);
        }

        fetchHistory();
    }, []);

    if (isLoading) {
        return <div>Loading...</div>
    }

    const exportFile = () => {
        let historyArray = [];
        const historyWithoutUserName = history.map(({username, ...rest}) => rest);


        historyArray.push(Object.keys(historyWithoutUserName[0])); // column names
        historyWithoutUserName.forEach(value => historyArray.push(Object.values(value))); // column values
        console.log(historyArray)

        const workSheet = XLSX.utils.aoa_to_sheet(historyArray);
        const workBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workBook, workSheet, (history[0].username).concat('-history'));
        XLSX.writeFile(workBook, (history[0].username).concat('-history.xlsx'))
    };

    const header = (
        <div>
            <h1 style={{margin: 0, textAlign: 'center'}}>History</h1>
            <h2 style={{margin: 5, textAlign: 'center', color: 'gold'}}>{history[0].username}</h2>
            <Button disabled={!history.length} className='p-button-info' onClick={exportFile}>
                <code>Export to XLSX</code>
            </Button>
        </div>
    );

    const footer = `In total there are ${history ? history.length : 0} rows.`;


    return (
        <div>
            <DataTable value={history} header={header} footer={footer}>
                <Column field="timestamp" header="Timestamp" sortable={true}/>
                <Column field="currencies" header="Currencies"/>
                <Column field="startDate" header="Start date" sortable={true}/>
                <Column field="endDate" header="End date" sortable={true}/>
            </DataTable>
        </div>
    );
};

export default History;