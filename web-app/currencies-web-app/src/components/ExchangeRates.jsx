import React, {useCallback, useEffect, useState} from "react";
import CurrenciesService from "../services/CurrenciesService";
import {MultiSelect} from "primereact/multiselect";
import {Calendar} from "primereact/calendar";
import {Button} from "primereact/button";
import CurrencyFlag from "react-currency-flags";
import {CartesianGrid, Legend, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import FileSaver from 'file-saver';
import {useCurrentPng} from "recharts-to-png";


const ExchangeRates = () => {
    console.log("ExchangeRates component called.")

    const dateFormat = require('dateformat');
    const COLORS = ['#03E3FC', '#03FC45', '#FCC603', '#FC03F8'];
    const [getPng, {ref: chartRef, isLoading: isDownloading}] = useCurrentPng({backgroundColor: '#323232'});

    const initialRate = {
        exchangeRates:
            [{
                unit: null,
                currency: null,
                dailyValues:
                    [{
                        date: null,
                        value: null
                    }]
            }],
    };

    const [currencies, setCurrencies] = useState(null);
    const [selectedCurrencies, setSelectedCurrencies] = useState([]);
    const [isCurrenciesLoading, setCurrenciesLoading] = useState(true);
    const [isRatesLoading, setRatesLoading] = useState(false);
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [rate, setRate] = useState(initialRate);
    const [data, setData] = useState(null);
    const [selectedData, setSelectedData] = useState(null);


    useEffect(() => {
        let curr = [];
        CurrenciesService.getCurrentExchangeRates().then(value => {
            value.data.rates.forEach(rates => {
                curr.push({name: rates.currency})
            });
            setCurrencies(curr);
            setCurrenciesLoading(false);
        });
    }, []);

    const onSubmit = (e) => {
        e.preventDefault();
        const selectedCurrenciesArray = selectedCurrencies.map(c => c.name);
        setRatesLoading(true);
        setData(null);
        CurrenciesService.getExchangeRates(selectedCurrenciesArray, dateFormat(startDate, "yyyy-mm-dd"), dateFormat(endDate, "yyyy-mm-dd")).then(value => {
            setRate(value.data)
            setData(createData(value.data));
            setSelectedData(selectedCurrenciesArray)
            setRatesLoading(false);
        });
    };

    const createData = (r) => {

        let data = {};

        r.exchangeRates.forEach(value => {

            value.dailyValues.forEach(dailyValue => {
                if (!data[dailyValue.date]) {
                    data[dailyValue.date] = {};
                }
                data[dailyValue.date]['date'] = dailyValue.date;
                data[dailyValue.date][value.currency] = dailyValue.value.replace(',', '.');
            })
        })

        const dataSet = [];
        const keys = Object.keys(data);
        keys.forEach(function (key) {
            dataSet.push(data[key]);
        });


        return dataSet.reverse();
    };


    const currencyTemplate = (option) => {
        return (
            <div className="p-multiselect-token">
                <CurrencyFlag currency={option.name} size="sm"/>
                <div className="p-multiselect-token-label">&nbsp;{option.name}</div>
            </div>
        );
    };

    const selectedCurrencyTemplate = (option) => {
        if (option) {
            return (
                <div className="p-multiselect-token">
                    <CurrencyFlag currency={option.name} size="sm"/>
                    <div className="p-multiselect-token-label">&nbsp;{option.name}</div>
                </div>
            );
        }
        return "Select Currencies";
    };

    const panelFooterTemplate = () => {
        const selectedItems = selectedCurrencies;
        const length = selectedItems ? selectedItems.length : 0;
        return (
            <div className="p-py-2 p-px-3">
                <b>{length}</b> item{length > 1 ? 's' : ''} of 4 items selected.
            </div>
        );
    };

    const handleDownload = useCallback(async () => {
        const png = await getPng();
        if (png) {
            FileSaver.saveAs(png, 'myChart.png');
        }
    }, [getPng]);


    return (
        <React.Fragment>
            <h1 style={{
                textAlign: 'center',
                marginBottom: 40,
                marginTop: 0,
                backgroundColor: '#191919',
                padding: 25
            }}>Currencies exchange rate chart</h1>
            <div>
                {!isCurrenciesLoading
                    ? <form onSubmit={onSubmit}
                            style={{
                                display: 'flex',
                                justifyContent: 'space-evenly',
                                alignItems: 'baseline',
                                flexWrap: 'wrap'
                            }}>
                        <MultiSelect
                            className="multiselect-custom"
                            value={selectedCurrencies}
                            options={currencies}
                            optionLabel="name"
                            onChange={e => setSelectedCurrencies(e.value)}
                            placeholder="Select a currency"
                            display="chip"
                            selectionLimit={4}
                            itemTemplate={currencyTemplate}
                            selectedItemTemplate={selectedCurrencyTemplate}
                            maxSelectedLabels={4}
                            panelFooterTemplate={panelFooterTemplate}
                            showSelectAll={false}
                            filter={true}
                            filterBy="name"
                            style={{width: 350}}
                        />
                        <div>
                            <Calendar style={{width: 300}} value={startDate} maxDate={new Date()}
                                      placeholder="Select a start date"
                                      onChange={(e) => setStartDate(e.value)} monthNavigator yearNavigator
                                      yearRange="2006:2021" readOnlyInput showIcon/>
                        </div>
                        <div>
                            <Calendar style={{width: 300}} minDate={startDate} maxDate={new Date()} value={endDate}
                                      placeholder="Select an end date"
                                      onChange={(e) => setEndDate(e.value)} monthNavigator yearNavigator
                                      yearRange="2006:2021" readOnlyInput showIcon/>
                        </div>
                        <Button style={{height: 60, width: 150, fontSize: 19, font: 'bold'}} type="submit"
                                label="Submit"
                                disabled={!selectedCurrencies.length || !startDate || !endDate || isRatesLoading}
                                className="p-button-raised"/>
                    </form>
                    : <h3 style={{textAlign: 'center', marginTop: 10}}>Loading...</h3>
                }
            </div>
            {!isRatesLoading && data
                ? <div>
                    {
                        !isDownloading
                            ? <Button onClick={handleDownload} className="p-button-info">
                                <code>Download Chart</code>
                            </Button>
                            : <Button disabled={true}>
                                <code>Downloading...</code>
                            </Button>
                    }
                    <ResponsiveContainer width="97%" minHeight={500}>
                        <LineChart
                            ref={chartRef}
                            data={data}
                            margin={{
                                top: 20,
                                bottom: 20
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3"/>
                            <XAxis dataKey="date"/>
                            <YAxis type="number"/>
                            <Tooltip/>
                            <Legend/>
                            {selectedData.map((data, index) =>
                                <Line
                                    key={data}
                                    type="monotone"
                                    dataKey={data}
                                    stroke={COLORS[index]}
                                    dot={false}
                                >
                                </Line>
                            )}
                        </LineChart>
                    </ResponsiveContainer>
                </div>
                : <h2 style={{textAlign: 'center', marginTop: 100, height: 500}}>No data</h2>
            }
        </React.Fragment>
    );
}

export default ExchangeRates;