import React from 'react';
import {useHistory} from "react-router-dom";
import {Menubar} from 'primereact/menubar';
import {Button} from "primereact/button";
import {connect} from "react-redux";
import {LogOutAuthAction} from "../redux/actions/AuthAction";

const Header = (props) => {
    const history = useHistory();
    const {auth, logout} = props;

    const items = [
        {
            label: 'Home',
            icon: 'pi pi-fw pi-home',
            command: () => {
                history.push("")
            }
        },
        {
            label: 'Currencies',
            icon: 'pi pi-fw pi-euro',
            items: [
                {
                    label: 'Info',
                    icon: 'pi pi-fw pi-info-circle',
                    command: () => {
                        history.push("/info")
                    }
                },
                {
                    label: 'Current Rates',
                    icon: 'pi pi-fw pi-star-o',
                    command: () => {
                        history.push("/currentExchangeRates")
                    }
                },
                {
                    label: 'My Rates',
                    icon: 'pi pi-fw pi-star',
                    command: () => {
                        history.push("/exchangeRates")
                    }
                }
            ]
        }
    ];

    return (
        <div>
            <Menubar
                model={items}
                end={auth.user
                    ? <React.Fragment>
                        <Button onClick={() => history.push("/history")} label="My History" icon="pi pi-book"
                                style={{marginRight: 20, fontSize: 18}} className="p-button-help"/>
                        <Button onClick={() => logout(history)} label="Sign Out" icon="pi pi-sign-out"
                                style={{marginRight: 10, fontSize: 18}} className="p-button-danger"/>
                    </React.Fragment>
                    : <React.Fragment>
                        <Button onClick={() => history.push("/register")} label="Register" icon="pi pi-id-card"
                                style={{marginRight: 20, fontSize: 18}} className="p-button-info"/>
                        <Button onClick={() => history.push("/signIn")} label="Sign In" icon="pi pi-sign-in"
                                style={{marginRight: 10, fontSize: 18}}/>
                    </React.Fragment>
                }
            />
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        auth: state.authState,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        logout: (history) => {
            dispatch(LogOutAuthAction(history));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);