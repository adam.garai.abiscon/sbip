package com.practice.demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.practice.demo.dto.Currencies;
import com.practice.demo.dto.ExchangeRates;
import com.practice.demo.service.CurrencyService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;

@Controller
@AllArgsConstructor
public class ThymeleafCurrencyController {

    private final CurrencyService currencyService;

    @GetMapping("/info")
    public String info(Model model) throws JsonProcessingException {

        Currencies currencies = currencyService.getCurrenciesInfo();
        model.addAttribute("currencies", currencies);

        return "info";
    }

    @GetMapping("/exchangeRates")
    public String exchangeRates(Model model,
                                @RequestParam(value = "startDate", required = false) String startDate,
                                @RequestParam(value = "endDate", required = false) String endDate,
                                @RequestParam(value = "curr", required = false) List<String> curr) throws JsonProcessingException {

        if (!curr.isEmpty()) {

            ExchangeRates exchangeRates = currencyService.getExchangeRates(startDate, endDate, curr);
            model.addAttribute("exchangeRates", exchangeRates);

        }
        return "exchangeRates";
    }
}
