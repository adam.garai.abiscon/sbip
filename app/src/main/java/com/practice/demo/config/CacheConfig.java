package com.practice.demo.config;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

import javax.cache.CacheManager;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.Duration;
import javax.cache.expiry.TouchedExpiryPolicy;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CacheConfig implements JCacheManagerCustomizer {
    public static final String CACHE_CURRENCY_INFO = "CACHE_CURRENCY_INFO";
    public static final String CACHE_CURRENCY_RATES = "CACHE_CURRENCY_RATES";

    @Override
    public void customize(CacheManager cacheManager) {
        cacheManager.destroyCache(CACHE_CURRENCY_INFO);
        cacheManager.createCache(CACHE_CURRENCY_INFO, createCacheConfiguration());
        cacheManager.destroyCache(CACHE_CURRENCY_RATES);
        cacheManager.createCache(CACHE_CURRENCY_RATES, createCacheConfiguration());
    }

    private MutableConfiguration<?, ?> createCacheConfiguration() {
        return new MutableConfiguration<>() //
                .setExpiryPolicyFactory(TouchedExpiryPolicy.factoryOf(new Duration(TimeUnit.DAYS, 1))) //
                .setStoreByValue(false) //
                .setManagementEnabled(true) //
                .setStatisticsEnabled(true); //
    }
}
