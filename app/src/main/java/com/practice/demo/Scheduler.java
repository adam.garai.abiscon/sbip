package com.practice.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.practice.demo.dto.ExchangeRate;
import com.practice.demo.dto.ExchangeRates;
import com.practice.demo.model.DailyRate;
import com.practice.demo.model.MonthlyRate;
import com.practice.demo.repository.DailyRateRepository;
import com.practice.demo.repository.MonthlyRateRepository;
import com.practice.demo.service.CurrencyService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.practice.demo.util.DateUtil.*;

@Slf4j
@AllArgsConstructor
@Component
public class Scheduler {

    private final CurrencyService currencyService;
    private final DailyRateRepository dailyRateRepository;
    private final MonthlyRateRepository monthlyRateRepository;

    @Scheduled(cron = "0 0/5 * * * ?")
    public void updateDailyRates() throws JsonProcessingException {

        log.info("updateDaily");

        LocalDate now = LocalDate.now();
        List<String> all = currencyService.getCurrenciesInfo().getCurrencies();
        all.remove("HUF");

        LocalDate firstDayOfMonth = YearMonth.from(now).atDay(1);
        ExchangeRates exchangeRates = currencyService.getExchangeRates(localDateToString(firstDayOfMonth), localDateToString(now), all);

        saveDailyRatesInOngoingMonth(now, exchangeRates);
    }

    public void calculateMonthlyRatesAverage() throws JsonProcessingException {

        LocalDate now = LocalDate.now();

        List<String> all = currencyService.getCurrenciesInfo().getCurrencies();
        all.remove("HUF");

        ExchangeRates exchangeRates = currencyService.getExchangeRates("2021-04-15", "2021-07-01", all);

        saveMonthlyRates(now, exchangeRates);

    }

    private void saveMonthlyRates(LocalDate now, ExchangeRates exchangeRates) {
        List<MonthlyRate> monthlyRateList = new ArrayList<>();

        exchangeRates.getExchangeRates().stream()
                .collect(Collectors.groupingBy(ExchangeRate::getCurrency)) // Map<String, List<ExchangeRate>
                .forEach((curr, exRates) -> exRates
                        .forEach(exchangeRate -> { // List<ExchangeRate> -> Map<YearMonth, List<DailyValue>>
                            exchangeRate.getDailyValues().stream()
                                    .filter(dailyValue -> !isOngoingMonth(dailyValue.getDate(), now))
                                    .collect(
                                            Collectors.groupingBy(dailyValue -> YearMonth.from(stringToLocalDate(dailyValue.getDate())))
                                    )
                                    .forEach((yearMonth, dailyValues) -> {
                                        List<BigDecimal> bigList = new ArrayList<>();

                                        dailyValues.stream()
                                                .map(dailyValue -> rateToBigDecimal(dailyValue.getValue()))
                                                .collect(Collectors.toCollection(() -> bigList));

                                        String lastDay = dailyValues.get(0).getDate();

                                        monthlyRateList.add(new MonthlyRate(curr, average(bigList), stringToLocalDate(lastDay)));
                                    });
                        })
                );
        monthlyRateRepository.saveAll(monthlyRateList);
    }

    private void saveDailyRatesInOngoingMonth(LocalDate now, ExchangeRates exchangeRates) {
        List<DailyRate> dailyRateList = new ArrayList<>();

        exchangeRates.getExchangeRates()
                .forEach(exchangeRate ->
                        exchangeRate.getDailyValues().stream()
                                .filter(dailyValue -> isOngoingMonth(dailyValue.getDate(), now))
                                .map(dailyValue -> new DailyRate(
                                                exchangeRate.getCurrency(),
                                                rateToBigDecimal(dailyValue.getValue()),
                                                stringToLocalDate(dailyValue.getDate())
                                        )
                                )
                                .collect(Collectors.toCollection(() -> dailyRateList))
                );

        if (now.equals(YearMonth.now().atDay(1))) {

            List<MonthlyRate> monthlyRateList = new ArrayList<>();
            List<DailyRate> databaseDailyRatesList = dailyRateRepository.findAll();

            databaseDailyRatesList.stream()
                    .collect(Collectors.groupingBy(DailyRate::getCurrency))
                    .forEach((curr, dailyRates) -> {

                        List<BigDecimal> bigList = new ArrayList<>();

                        dailyRateList.stream()
                                .map(DailyRate::getRate)
                                .collect(Collectors.toCollection(() -> bigList));


                        monthlyRateList.add(new MonthlyRate(curr, average(bigList), now));
                    });

            monthlyRateRepository.saveAll(monthlyRateList);
        }
        dailyRateRepository.deleteAll();
        dailyRateRepository.saveAll(dailyRateList);
    }

    private BigDecimal rateToBigDecimal(String dailyValue) {
        Locale aDefault = Locale.forLanguageTag("hu-HU");
        try {
            double doubleValue = NumberFormat.getNumberInstance(aDefault).parse(dailyValue).doubleValue();
            return new BigDecimal(doubleValue).setScale(2, RoundingMode.DOWN);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return BigDecimal.ZERO;
    }

    private BigDecimal average(List<BigDecimal> bigDecimals) {
        BigDecimal sum = bigDecimals.stream()
                .map(Objects::requireNonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return sum.divide(new BigDecimal(bigDecimals.size()), 2, RoundingMode.DOWN);
    }

}
