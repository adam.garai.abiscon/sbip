package com.practice.demo.service;

import com.practice.demo.model.History;
import com.practice.demo.repository.HistoryRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class HistoryService {

    private final HistoryRepository historyRepository;

    public List<History> getHistory(String userName) {
        return historyRepository.findAllByUsername(userName);
    }
}
